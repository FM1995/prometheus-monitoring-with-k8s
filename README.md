# Prometheus monitoring with K8s

#### Project Outline

In this project we will setup our cluster on EKS and deploy Prometheus in it
There usually three ways to deploy Promethees with Kubernetes

1.	Can create the configuration YAML files independently and execute them in the right order

2.	Second one using an Operator and deploy into a K8s cluster which is the manager of Prometheus components

3.	The next one is using HELM chart to deploy the operator, so helm will manage the initial setup and Operator will manage the running Prometheus setup

So in this project we will be using HELM to deploy our Prometheus cluster and we will create a cluster in EKS, and deploy our online-shop-microservices application and then deploy our Prometheus stack.


#### Lets get started

First step lets create the cluster on EKS

First configure the credentials and region

![Image 1](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image1.png)

Can then run the below command

```
eksctl create cluster
```

The above will deploy a Kubernetes cluster on AWS EKS, taking care of the underlying infrastructure setup and integration with AWS services with default settings.

![Image 2](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image2.png)

And can see it is ready

![Image 3](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image3.png)

Can then run the below to get the cluster nodes running

```
kubectl get node
```

![Image 4](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image4.png)

Now we can deploy our Microservices application

![Image 6](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image6.png)

Can now run the below

```
kubectl apply -f config-microservices.yaml
```

And can see the microservices deployed

![Image 7](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image7.png)

Can then run the below to get the pods

```
kubectl get pod
```

![Image 8](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image8.png)

Now lets deploy the Promethues monitoring stack using the helm chart


Using the below link

https://github.com/prometheus-community/helm-charts

Let’s add the helm repository

![Image 9](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image9.png)

```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
```

![Image 10](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image10.png)

And then run the update

```
helm repo update
```

![Image 11](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image11.png)

Now lets separate the Prometheus stack and microservice application

Let’s install the monitoring stack in it’s own namespace

Create namespace called monitoring

```
kubectl create namespace monitoring
```

![Image 12](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image12.png)

Now lets install the helm chart, Can also run the below to see which stack to be used

```
helm search repo prometheus-community
```

And in our case it will be kube-prometheus-stack

![Image 13](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image13.png)

Can then use the below command

```
helm install prometheus-monitoring prometheus-community/kube-prometheus-stack -n monitoring
```

And can see the monitoring stack got deployed

![Image 14](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image14.png)

Can then see the pods

![Image 15](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image15.png)

Can also see the stack components in the namespace

![Image 16](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image16.png)

Now let’s check the components inside Prometheus, Alertmanager and Operator

Can print out the stateful set

```
kubectl get statefulset -n monitoring
```

![Image 17](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image17.png)

Can also describe the stateful sets and save them to a respective file

```
kubectl describe statefulset prometheus-prometheus-monitoring-kube-prometheus -n monitoring > prom.yaml

kubectl describe statefulset alertmanager-prometheus-monitoring-kube-alertmanager -n monitoring > alert.yaml

kubectl describe deployment prometheus-monitoring-kube-operator -n monitoring > operator.yaml
```

![Image 18](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image18.png)

Checking the stateful set for the Prometheus from the file ‘prom.yaml’

Mounts describe what endpoints to scrape and which addresses aswell as the metrics to be exposed.

![Image 19](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image19.png)

We also have the config reloader

Which is responsible for re-loading the configuration files changes, so when we add a new target or endpoint it will automatically add it without the need for a restart

It will also re-load the changes to the rules file when we delete or update the rules file

![Image 20](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image20.png)

Can also check the confimaps and can see we get the default rules file

![Image 21](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image21.png)

And can then export it to another file like config.yaml

```
kubectl get configmap -n monitoring prometheus-prometheus-monitoring-kube-prometheus-rulefiles-0 -o yaml > config.yaml
```


![Image 22](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image22.png)


Can then also see the rules file

![Image 23](https://gitlab.com/FM1995/prometheus-monitoring-with-k8s/-/raw/main/Images/Image23.png)








